class WorkoutCache {
  String title;
  String wtype;
  DateTime dateTime;
  Duration duration;
  double distance;
  Duration averagePace;
  double averageSpeed;
  String path;

  WorkoutCache(this.title, this.wtype, this.dateTime, this.duration,
      this.distance, this.averagePace, this.averageSpeed, this.path);

  WorkoutCache.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        wtype = json['type'],
        dateTime = DateTime.parse(json['datetime']),
        duration = Duration(milliseconds: json['duration'].toInt()),
        distance = json['distance'].toDouble(),
        averagePace = Duration(milliseconds: json['averagePace'].toInt()),
        averageSpeed = json['averageSpeed'].toDouble(),
        path = json['path'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'type': wtype,
        'dateTime': dateTime.toIso8601String(),
        'duration': duration.inMilliseconds,
        'distance': distance,
        'averagePace': averagePace.inMilliseconds,
        'averageSpeed': averageSpeed,
        'path': path
      };

  WorkoutCache.fromMap(dynamic obj) {
    this.title = obj['title'];
    this.wtype = obj['wtype'];
    this.dateTime = DateTime.fromMillisecondsSinceEpoch(obj['datetime']);
    this.duration = Duration(milliseconds: obj['duration']);
    this.distance = obj['distance'].toDouble();
    this.averagePace = Duration(milliseconds: obj['averagePace']);
    this.averageSpeed = obj['averageSpeed'].toDouble();
    this.path = obj['path'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["wtype"] = wtype;
    map["datetime"] = dateTime.millisecondsSinceEpoch.abs();
    map["duration"] = duration.inMilliseconds.abs();
    map["distance"] = distance;
    map["averagePace"] = averagePace.inMilliseconds.abs();
    map["averageSpeed"] = averageSpeed;
    map["path"] = path;
    return map;
  }
}
