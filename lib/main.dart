import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'package:gpx/gpx.dart';
import 'package:slider_button/slider_button.dart';
import 'package:intl/intl.dart';
import 'storage.dart';
import 'package:rounded_letter/rounded_letter.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:charts_flutter/flutter.dart' as charts;
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'session.dart';

const Color notWhite = Color(0xFFEDF0F2);

/*final topAppBar = AppBar(
  elevation: 0.1,
  backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
  title: Text('OhMy'),
  actions: <Widget>[
    //IconButton(
    //  icon: Icon(Icons.list),
    // onPressed: () {},
    //)
  ],
);*/

void main() {
  runApp(MaterialApp(
    title: 'Ohmy',
    theme: ThemeData(
      // This is the theme of your application.
      //
      // Try running your application with "flutter run". You'll see the
      // application has a blue toolbar. Then, without quitting the app, try
      // changing the primarySwatch below to Colors.green and then invoke
      // "hot reload" (press "r" in the console where you ran "flutter run",
      // or simply save your changes to "hot reload" in a Flutter IDE).
      // Notice that the counter didn't reset back to zero; the application
      // is not restarted.
      primarySwatch: Colors.blue,
      primaryColor: Colors.blue,
      bottomAppBarColor: Color(0xFFEDF0F2),

      appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: Colors.black),
          color: Colors.transparent),
      // This makes the visual density adapt to the platform that you run
      // the app on. For desktop platforms, the controls will be smaller and
      // closer together (more dense) than on mobile platforms.
      visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/': (context) => Dashboard(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/recording': (context) => Recording(),
      '/workout': (context) => WorkoutView()
    },

    //home: Dashboard(),
  ));
}

class Dashboard extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DashboardPage();
  }
}

class WorkoutView extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final WorkoutCache resume = ModalRoute.of(context).settings.arguments;

    return WorkoutPage(resume: resume);
  }
}

class Recording extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RecordingPage(storage: GPXStorage());
  }
}

class DashboardPage extends StatefulWidget {
  DashboardPage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class WorkoutPage extends StatefulWidget {
  final WorkoutCache resume;
  WorkoutPage({Key key, @required this.resume}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _WorkoutPageState createState() => _WorkoutPageState();
}

class RecordingPage extends StatefulWidget {
  final GPXStorage storage;

  RecordingPage({Key key, @required this.storage}) : super(key: key);

  @override
  _RecordingPageState createState() => _RecordingPageState();
}

class TimeSeriesSpeed {
  final DateTime time;
  final double speed;

  TimeSeriesSpeed(this.time, this.speed);
}

void _settingWorkoutModalBottomSheet(
    context, GPXStorage storage, WorkoutCache resume) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          child: new Wrap(
            children: <Widget>[
              new ListTile(
                  leading: new Icon(Icons.delete),
                  title: new Text('Delete'),
                  onTap: () {
                    storage.deleteGPX(resume.path);
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    //Navigator.pushReplacementNamed(context, '/').then((rt) {
                    //  Navigator.of(context).pop();
                    //});
                  }),
              new ListTile(
                leading: new Icon(Icons.mode_edit),
                title: new Text('Edit Title'),
                onTap: () => {},
              ),
              new ListTile(
                leading: new Icon(Icons.mode_edit),
                title: new Text('Edit Type'),
                onTap: () => {},
              ),
              new ListTile(
                leading: new Icon(Icons.mode_edit),
                title: new Text('Edit Duration'),
                onTap: () => {},
              ),
              new ListTile(
                leading: new Icon(Icons.mode_edit),
                title: new Text('Edit Distance'),
                onTap: () => {},
              ),
              new ListTile(
                leading: new Icon(Icons.share),
                title: new Text('Share'),
                onTap: () => {},
              ),
            ],
          ),
        );
      });
}

class _WorkoutPageState extends State<WorkoutPage> {
  WorkoutCache resume;
  final GPXStorage storage = GPXStorage();
  Gpx gpx;
  List<LatLng> points = [];
  LatLng mapCenter = LatLng(48, 2);
  LatLngBounds mapBounds = LatLngBounds();
  MapController mapController;
  final List<TimeSeriesSpeed> speedList = [];
  //List<charts.Series> seriesList = [];
  List<charts.Series<TimeSeriesSpeed, DateTime>> seriesList;

  @override
  void initState() {
    super.initState();
    mapController = MapController();

    Wpt wpt;
    // Load GPX
    // FIXME
    resume = widget.resume;
    storage.readGPXFilepath(resume.path).then((gpx) {
      this.gpx = gpx;
      print(gpx.trks.first.trksegs.first.trkpts);
      this.setState(() {
        //this.mapCenter = new LatLng(
        for (var trksIndex = 0; trksIndex < gpx.trks.length; trksIndex++) {
          for (var trkSegmentIndex = 0;
              trkSegmentIndex < gpx.trks[trksIndex].trksegs.length;
              trkSegmentIndex++) {
            for (var wptsIndex = 0;
                wptsIndex <
                    gpx.trks[trksIndex].trksegs[trkSegmentIndex].trkpts.length -
                        1;
                wptsIndex++) {
              wpt = gpx
                  .trks[trksIndex].trksegs[trkSegmentIndex].trkpts[wptsIndex];
              points.add(LatLng(wpt.lat, wpt.lon));

              if ((wptsIndex + 1) <
                  (gpx.trks[trksIndex].trksegs[trkSegmentIndex].trkpts.length -
                      1)) {
                speedList.add(new TimeSeriesSpeed(
                    wpt.time,
                    storage.computeSpeedBetween(
                        wpt,
                        gpx.trks[trksIndex].trksegs[trkSegmentIndex]
                            .trkpts[wptsIndex + 1])));
              }
            }
          }
        }

        seriesList = [
          new charts.Series<TimeSeriesSpeed, DateTime>(
            id: 'Sales',
            colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
            domainFn: (TimeSeriesSpeed speeds, _) => speeds.time,
            measureFn: (TimeSeriesSpeed speeds, _) => speeds.speed,
            data: speedList,
          )
        ];
        // Compute Map center
        // FIXME (should use bounding box)
        try {
          mapBounds = new LatLngBounds.fromPoints(points);
          print(mapBounds);
          mapController.fitBounds(mapBounds);

          // ,{
          // options: FitBoundsOptions(
          // padding: EdgeInsets.only(left: 15.0, right: 15.0),
          // })

          /*mapCenter = new LatLng(
            (mapBounds.north + mapBounds.south) / 2,
            (mapBounds.west + mapBounds.east) / 2,
          )*/
          print(mapCenter);
        } catch (e) {
          print(e);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.

          //backgroundColor: Colors.transparent,
          //iconTheme: IconThemeData(color: Colors.blueGrey),
          elevation: 0,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    _settingWorkoutModalBottomSheet(
                        context, storage, this.resume);
                  },
                  child: Icon(Icons.more_vert),
                )),
          ],
          title: Text('${this.resume.title}',
              style: Theme.of(context).textTheme.headline5),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 2,
            child: FlutterMap(
              mapController: mapController,
              options: new MapOptions(
                center: mapCenter,
                zoom: 9.0,
              ),
              layers: [
                new TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']),
                PolylineLayerOptions(
                  polylines: [
                    Polyline(
                        points: points, strokeWidth: 4.0, color: Colors.purple),
                  ],
                ),
                /*new MarkerLayerOptions(
              markers: [
                new Marker(
                  width: 80.0,
                  height: 80.0,
                  point: new LatLng(51.5, -0.09),
                  builder: (ctx) => new Container(
                    child: new FlutterLogo(),
                  ),
                ),
              ],
            ),*/
              ],
            ),
          ),
          Container(
              color: notWhite,
              //mainAxisAlignment: MainAxisAlignment.center,
              padding: EdgeInsets.all(10.0),
              child: Text(
                "${timeago.format(resume.dateTime)}",
                textAlign: TextAlign.center,
              )),
          Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                        flex: 5,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "${resume.duration.inHours.toString().padLeft(2, '0')}h${resume.duration.inMinutes.remainder(60).toString().padLeft(2, '0')}m",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text('Duration',
                                  style: Theme.of(context).textTheme.caption)
                            ])),
                    Expanded(
                        flex: 5,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "${resume.averagePace.inMinutes.toString().padLeft(2, '0')}:${resume.averagePace.inSeconds.remainder(60).toString().padLeft(2, '0')}",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text(
                                'Average Pace (min/km)',
                                style: Theme.of(context).textTheme.caption,
                              ),
                            ])),
                  ])),
          Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                        flex: 5,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                "${(resume.distance / 1000).toStringAsFixed(2)}",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text('Distance (km)',
                                  style: Theme.of(context).textTheme.caption)
                            ])),
                    Expanded(
                        flex: 5,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "${resume.averageSpeed.toStringAsFixed(2)}",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text(
                                'Average Speed (km/h)',
                                style: Theme.of(context).textTheme.caption,
                              ),
                            ])),
                  ])),
          /*charts.TimeSeriesChart(
            seriesList,
            animate: false,
            // Configure the default renderer as a line renderer. This will be used
            // for any series that does not define a rendererIdKey.
            //
            // This is the default configuration, but is shown here for  illustration.
            defaultRenderer: new charts.LineRendererConfig(),
            // Custom renderer configuration for the point series.
            customSeriesRenderers: [
              new charts.PointRendererConfig(
                  // ID used to link series to this renderer.
                  customRendererId: 'customPoint')
            ],
            // Optionally pass in a [DateTimeFactory] used by the chart. The factory
            // should create the same type of [DateTime] as the data provided. If none
            // specified, the default creates local date time.
            dateTimeFactory: const charts.LocalDateTimeFactory(),
          )*/
        ]));
    // Center is a layout widget. It takes a single child and positions it
    // in the middle of the parent.
    //  );
  }
}

class ListLinearProgressIndicator extends LinearProgressIndicator
    implements PreferredSizeWidget {
  ListLinearProgressIndicator({
    Key key,
    double value,
    Color backgroundColor,
    Animation<Color> valueColor,
  }) : super(
          key: key,
          value: value,
          backgroundColor: backgroundColor,
          valueColor: valueColor,
        ) {
    preferredSize = Size(double.infinity, 6.0);
  }

  @override
  Size preferredSize;
}

class _DashboardPageState extends State<DashboardPage> {
  WorkoutsIndex index;
  bool _loading = true;

  void _incrementCounter() {
    //setState(() {
    // This call to setState tells the Flutter framework that something has
    // changed in this State, which causes it to rerun the build method below
    // so that the display can reflect the updated values. If we changed
    // _counter without calling setState(), then the build method would not be
    // called again, and so nothing would appear to happen.
    //});

    Navigator.pushReplacementNamed(context, '/recording').then((idx) {});
  }

  void refreshIndex() {
    _loading = true;

    index.createIndex().then((index) {
      setState(() {
        this.index.resumes = index;
        this._loading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    // Load Index
    index = WorkoutsIndex();
    index.load().then((index) {
      setState(() {
        this.index = index;
      });
      this.refreshIndex();
    });
  }

  ListTile makeListTile(WorkoutCache w) => ListTile(
      leading: Column(
        children: <Widget>[
          Icon(
            Icons.directions_run,
            color: Colors.blue,
          ),
          SizedBox(
            height: 5.0,
          ),
          RoundedLetter(
            text: "${(w.distance / 1000).toStringAsFixed(0)}",
            shapeColor: Colors.green,
            shapeSize: 20,
            fontSize: 10,
          ),
        ],
      ),
      title: Text(
        "${w.title} ",
      ),
      subtitle: Text(
          "${w.duration.inHours.toString().padLeft(2, '0')}h${w.duration.inMinutes.remainder(60).toString().padLeft(2, '0')}m - ${DateFormat().format(w.dateTime)}\n${(w.distance / 1000).toStringAsFixed(2)} km - ${w.averageSpeed.toStringAsFixed(2)} km/h - ${w.averagePace.inMinutes.toString().padLeft(2, '0')}:${w.averagePace.inSeconds.remainder(60).toString().padLeft(2, '0')} min/km"),
      trailing: Icon(
        Icons.arrow_forward_ios,
      ),
      isThreeLine: true,
      onTap: () {
        Navigator.pushNamed(context, '/workout', arguments: w).then((r) {
          this.refreshIndex();
        });
      });

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text('OhMy', style: Theme.of(context).textTheme.headline5),
        bottom: ListLinearProgressIndicator(
            backgroundColor: notWhite, value: (this._loading ? null : 0.0)),
      ),
      body:
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          ListView.builder(
              itemCount: this.index.resumes.length,
              itemBuilder: (context, idx) {
                return makeListTile(this.index.resumes[idx]);
              }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 4.0,
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.scatter_plot),
              onPressed: () {},
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class _RecordingPageState extends State<RecordingPage> {
  String _accuracy = "waiting...";
  String _speed = "waiting...";
  String _distance = "waiting...";
  String _elapsed = "waiting...";

  double totalDistanceInM = 0;
  DateTime startTime = new DateTime.now();

  List<dynamic> points = [];

  @override
  void initState() {
    super.initState();
    BackgroundLocation.startLocationService();
    BackgroundLocation.getLocationUpdates(this._onLocation);
  }

  void _onLocation(location) {
    //this.latitude = location.latitude.toString();
    //this.longitude = location.longitude.toString();
    print(location.time);
    this.points.add({
      "lat": location.latitude,
      "lon": location.longitude,
      "acc": location.accuracy,
      "alt": location.altitude,
      "speed": location.speed,
      "time": location.time.toInt()
    });
    var stats = GPXStorage().computeStatsOnPoints(this.points);
    totalDistanceInM = stats['distance'];
    this.setState(() {
      this._elapsed =
          Duration(milliseconds: location.time.toInt() - this.points[0]["time"])
              .toString()
              .split('.')
              .first
              .padLeft(8, "0");
      this._accuracy = location.accuracy.toStringAsFixed(1);
      //this.altitude = location.altitude.toString();
      //this.bearing = location.bearing.toString();
      this._speed = (location.speed * 3.6).toStringAsFixed(1) + ' km/h';
      this._distance = (totalDistanceInM / 1000).toStringAsFixed(2) + ' km';
    });
  }

  void _saveGpx() {
    try {
      widget.storage
          .writeGPX(this.points, this.points[0]["time"].toString() + ".gpx");
    } catch (e) {
      print(e);
    }
  }

  void _stopAndRecord() {
    print("STOPPED");

    BackgroundLocation.stopLocationService();
    this._saveGpx();

    //FIXME UPDATE LIST
    Navigator.pushReplacementNamed(context, '/');
    /*Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => _DashboardPageState),
    );*/
  }

  @override
  void dispose() {
    BackgroundLocation.stopLocationService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  this._elapsed,
                  style: Theme.of(context).textTheme.headline3,
                ),
                Text('Elapsed Time', style: Theme.of(context).textTheme.caption)
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                    this._distance,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text('Distance', style: Theme.of(context).textTheme.caption)
                ]),
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text(
                    this._speed,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Text(
                    'Speed',
                    style: Theme.of(context).textTheme.caption,
                  ),
                ]),
              ]),
              Text(
                'Accuracy : ' + this._accuracy,
                style: Theme.of(context).textTheme.bodyText2,
              ),
              SliderButton(
                height: 40,
                buttonColor: Theme.of(context).primaryColor,
                action: () {
                  ///Do something here
                  //Navigator.of(context).pop();
                  _stopAndRecord();
                },
                label: Text(
                  "Slide to stop",
                  style: TextStyle(
                      color: Color(0xff4a4a4a),
                      fontWeight: FontWeight.w500,
                      fontSize: 17),
                ),
                icon: Text(
                  "x",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w200,
                    fontSize: 32,
                  ),
                ),
              )
            ]),
      ),

      /*    floatingActionButton: FloatingActionButton(
        onPressed: _stopAndRecord,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
