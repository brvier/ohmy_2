import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:gpx/gpx.dart';
import 'package:geo/geo.dart';
import 'package:http/http.dart' as http;
import 'session.dart';
import 'query.dart';
import 'database_helper.dart';

class GPXStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<String> get _extLocalPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  Future<File> _localFile(String filename) async {
    final path = await _localPath;
    return File('$path/$filename');
  }

  Future<File> _extLocalFile(String filename) async {
    final path = await _extLocalPath;
    return File('$path/$filename');
  }

  Future<Gpx> readGPXFilename(String filename) async {
    try {
      final file = await _extLocalFile(filename);

      // Read the file
      String contents = await file.readAsString();

      return GpxReader().fromString(contents);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Gpx> readGPXFilepath(String filepath) async {
    try {
      final file = File(filepath);

      // Read the file
      String contents = await file.readAsString();

      return GpxReader().fromString(contents);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Gpx> readGPXFile(File file) async {
    try {
      // Read the file
      String contents = await file.readAsString();

      return GpxReader().fromString(contents);
    } catch (e) {
      print(e);
      return null;
    }
  }

  double computeSpeedBetween(Wpt one, Wpt two) {
    var distance = computeDistanceBetween(
        LatLng(one.lat, one.lon), LatLng(two.lat, two.lon));
    var duration = (two.time.difference(one.time)).inMilliseconds * 1000;
    if (duration == 0) {
      return 0.0;
    }

    return distance / duration;
  }

  Map<String, dynamic> computeStatsOnTrks(List<Trk> trks) {
    Map<String, dynamic> stats = {
      'distance': 0.0,
      'averagePace': 0.0,
      'averageSpeed': 0.0,
      'duration': Duration(seconds: 0)
    };
    num distance = 0;
    Wpt wpt, wptn1;

    try {
      for (var trksIndex = 0; trksIndex < trks.length; trksIndex++) {
        for (var trkSegmentIndex = 0;
            trkSegmentIndex < trks[trksIndex].trksegs.length;
            trkSegmentIndex++) {
          for (var wptsIndex = 0;
              wptsIndex <
                  trks[trksIndex].trksegs[trkSegmentIndex].trkpts.length - 1;
              wptsIndex++) {
            wpt = trks[trksIndex].trksegs[trkSegmentIndex].trkpts[wptsIndex];
            wptn1 =
                trks[trksIndex].trksegs[trkSegmentIndex].trkpts[wptsIndex + 1];
            distance += computeDistanceBetween(
                LatLng(wpt.lat, wpt.lon), LatLng(wptn1.lat, wptn1.lon));
          }
        }
      }
    } catch (e) {
      print(e);
    }
    stats['distance'] = distance.toDouble();

    try {
      stats['duration'] = trks.last.trksegs.last.trkpts.last.time
          .difference(trks.first.trksegs.first.trkpts.first.time);
      //Duration(
      //   milliseconds:
      //      (trks.last.trksegs.last.trkpts.last.time.millisecondsSinceEpoch -
      //         trks.first.trksegs.first.trkpts.first.time
      //            .millisecondsSinceEpoch));
    } catch (e) {
      stats['duration'] = Duration(seconds: 0);
    }

    try {
      stats['averagePace'] = Duration(
          seconds: (stats['duration'].inSeconds / (stats['distance'] / 1000))
              .toInt());
    } catch (e) {
      stats['averagePace'] = Duration(seconds: 0);
    }

    if (distance > 0) {
      try {
        stats['averageSpeed'] =
            (stats['distance'] / 1000) / (stats['duration'].inSeconds / 3600);
      } catch (e) {
        stats['averageSpeed'] = 0.0;
      }
    }
    return stats;
  }

  Map<String, dynamic> computeStatsOnPoints(List<dynamic> points) {
    Map<String, dynamic> stats = {
      'distance': 0.0,
      'averagePace': 0.0,
      'averageSpeed': 0.0,
      'duration': Duration(seconds: 0)
    };
    num distance = 0.0;
    try {
      for (var i = 0; i < points.length - 2; i++) {
        distance += computeDistanceBetween(
            LatLng(points[i]["lat"], points[i]["lon"]),
            LatLng(points[i + 1]["lat"], points[i + 1]["lon"]));
      }
    } catch (e) {
      print(e);
    }
    stats['distance'] = distance;

    stats['duration'] =
        Duration(milliseconds: (points.last["time"] - points.first["time"]));

    try {
      stats['averagePace'] = Duration(
          milliseconds:
              (stats['duration'].inMilliseconds / (stats['distance'] / 1000.0))
                  .toInt());
    } catch (e) {
      print(e);
    }
    return stats;
  }

  Future deleteGPX(String filepath) async {
    await File(filepath).delete();
    await QueryCtr().deleteWorkoutCache(filepath);
  }

  Future<File> writeGPX(List<dynamic> points, String filename) async {
    final file = await _extLocalFile(filename);

    var gpx = Gpx();
    gpx.creator = "OhMy";
    List<Wpt> wpts = [];

    gpx.metadata = new Metadata();
    try {
      gpx.metadata.name =
          await createTitle(gpx.trks.first.trksegs.first.trkpts.first);
    } catch (e) {}

    try {
      gpx.metadata.time =
          DateTime.fromMillisecondsSinceEpoch(points.first["time"]);
    } catch (e) {
      print(e);
    }
    for (var i = 0; i < points.length - 1; i++) {
      wpts.add(Wpt(
        lat: points[i]["lat"],
        lon: points[i]["lon"],
        ele: points[i]["ele"],
        time: DateTime.fromMillisecondsSinceEpoch(points[i]["time"]),
        hdop: points[i]["accuracy"],
      )
          //speed: this.point[i]["speed"]);
          );
    }

    gpx.trks = [
      Trk(trksegs: [Trkseg(trkpts: wpts)]),
    ];

    // Write the file
    print('Write as string $filename');
    print(GpxWriter().asString(gpx, pretty: true));
    return file.writeAsString(GpxWriter().asString(gpx, pretty: true));
  }

  Future<List<FileSystemEntity>> listFile() async {
    final path = await _extLocalPath;
    final dir = Directory(path);
    return dir.listSync().toList();
  }

  Future<String> createTitle(Wpt point) async {
    //https://nominatim.openstreetmap.org/reverse?lat=48.8524207&lon=2.3736585&format=json
    var suburb = await http.get(
        'https://nominatim.openstreetmap.org/reverse?lat=${point.lat}&lon=${point.lon}&format=json');
    return "${json.decode(suburb.body)['address']['town']}";
  }

  Future<WorkoutCache> resumeGPX(File file) async {
    final gpx = await readGPXFile(file);
    DateTime dt = DateTime(0);
    String title = "";
    var stats;

    try {
      stats = computeStatsOnTrks(gpx.trks);
    } catch (e) {
      stats = {
        'duration': Duration(),
        'distance': 0.0,
        'averagePace': Duration(),
        'averageSpeed': 0.0
      };
      print(e);
    }

    try {
      if (gpx.metadata.name != null) {
        title = gpx.metadata.name;
      }
    } catch (e) {
      title = "";
    }

    try {
      title = await createTitle(gpx.trks.first.trksegs.first.trkpts.first);
    } catch (e) {
      print(e);
    }

    try {
      dt = gpx.trks.first.trksegs.first.trkpts.first.time;
      print(gpx.trks.first.trksegs.first.trkpts.first.time.toIso8601String());
    } catch (e) {}

    return WorkoutCache(title, 'Run', dt, stats['duration'], stats['distance'],
        stats['averagePace'], stats['averageSpeed'], file.path);
  }
}

class WorkoutsIndex {
  List<WorkoutCache> resumes = [];

  WorkoutsIndex();
  Future<WorkoutsIndex> save() async {
    return this;
  }

  Future<List<WorkoutCache>> createIndex() async {
    var gpxs = await GPXStorage().listFile();
    GPXStorage storage = GPXStorage();
    WorkoutCache wc;
    print("Create Index");
    List<WorkoutCache> wcs = await QueryCtr().getAllWorkouts();

    for (var wc in wcs) {
      bool exts = await File(wc.path).exists();
      if (!exts) {
        await QueryCtr().deleteWorkoutCache(wc.path);
      }
    }

    List<WorkoutCache> ws = [];
    for (var path in gpxs) {
      if ((path is File) && (path.path.endsWith('.gpx'))) {
        if ((wcs.singleWhere((it) => it.path == path.path,
                orElse: () => null)) !=
            null) {
        } else {
          wc = await storage.resumeGPX(path);
          if (wc.dateTime != null) {
            ws.add(wc);
            QueryCtr().insertWorkoutCache(wc);
          }
        }
      }
    }
    wcs.addAll(ws);
    wcs.sort((a, b) => b.dateTime.compareTo(a.dateTime));
    return wcs;
  }

  Future<WorkoutsIndex> load() async {
    try {
      this.resumes = await QueryCtr().getAllWorkouts();
    } catch (e) {
      print("Cant load sql index");
      this.resumes = [];
      //this.createIndex();
    }
    // Refresh index anyway
    /*this.createIndex().then((resumes) {
      print("resumes");
      print(resumes);
      this.resumes = resumes;
    });*/
    return this;
  }
}

/*
class WorkoutResume {
  final String title;
  final String wtype;
  final DateTime dateTime;
  final Duration duration;
  final double distance;
  final Duration averagePace;
  final double averageSpeed;
  final String path;

  WorkoutResume(this.title, this.wtype, this.dateTime, this.duration,
      this.distance, this.averagePace, this.averageSpeed, this.path);

  WorkoutResume.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        wtype = json['type'],
        dateTime = DateTime.parse(json['datetime']),
        duration = Duration(milliseconds: json['duration'].toInt()),
        distance = json['distance'].toDouble(),
        averagePace = Duration(milliseconds: json['averagePace'].toInt()),
        averageSpeed = json['averageSpeed'].toDouble(),
        path = json['path'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'type': wtype,
        'dateTime': dateTime.toIso8601String(),
        'duration': duration.inMilliseconds,
        'distance': distance,
        'averagePace': averagePace.inMilliseconds,
        'averageSpeed': averageSpeed,
        'path': path
      };
}*/
