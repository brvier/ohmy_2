import 'dart:io';
import 'package:path/path.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;
  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();
  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "data_flutter.db");

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS workouts (path TEXT PRIMARY KEY, wtype TEXT, datetime INTEGER, duration INTEGER, distance INTEGER, averagePace INTEGER, averageSpeed INTEGER, title TEXT);');

      await db.execute('CREATE INDEX datetime_idx ON workouts(datetime);');
      await db.execute('CREATE INDEX path_idx ON workouts(path);');
    });

    return database;
  }
}
