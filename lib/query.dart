import 'dart:async';
import 'database_helper.dart';
import 'session.dart';

class QueryCtr {
  DatabaseHelper con = new DatabaseHelper();

  Future<List<WorkoutCache>> getAllWorkouts() async {
    var dbClient = await con.db;
    var res = await dbClient.query("workouts", orderBy: '"datetime" DESC');

    List<WorkoutCache> list =
        res.isNotEmpty ? res.map((c) => WorkoutCache.fromMap(c)).toList() : [];

    return list;
  }

  Future<int> insertWorkoutCache(WorkoutCache w) async {
    var db = await con.db;

    var result = await db.insert("workouts", w.toMap());

    return result;
  }

  Future<int> deleteWorkoutCache(String path) async {
    var db = await con.db;

    var result =
        await db.delete("workouts", where: 'path = ?', whereArgs: [path]);

    return result;
  }
}
